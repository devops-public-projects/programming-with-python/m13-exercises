
# Automation with Python Scripts
Bootcamp projects for Techworld with Nana Module 13.

## Technologies Used
- Python
- IntelliJ
- GitLab API
- 3rd Party Python Library **openpyxl**

## Project Descriptions

### Project 1 | Countdown App
- Take an input with a goal and deadline for said goal
- Print out the time delta between today and the goal deadline

### Project 2 | Spreadsheet Automation
- Reads a Spreadsheet file
- Manipulates data
	- Get a list of all suppliers
	- Determine each suppliers inventory
	- Calculate pricing of all goods and total value of each supplier
- Print all calculated results
- Save the modified spreadsheet to a new file

### Project 3 | GitLab API Request
- Talk to GitLab API and list all projects for a specific user