from datetime import datetime

def main() -> None:
    goal = input("Enter your goal: ").strip()
    deadline = get_deadline()
    today = datetime.today()

    time_til_deadline = get_time_til_deadline(deadline, today)

    deadline_string = r'Deadline for goal "%s" is in; %d days, %d hours, and %d minutes!' % (goal,*time_til_deadline)
    print(deadline_string)

def get_time_til_deadline(deadline: datetime, today: datetime) -> list:
    time_til_deadline = deadline - today
    hours = time_til_deadline.seconds // 3600
    minutes = (time_til_deadline.seconds//60) % 60

    return [time_til_deadline.days, hours, minutes]

def get_deadline() -> datetime:
    deadline = input("Enter your goal deadline (MM/DD/YYYY): ").strip()
    deadline_split = list(map(int,(deadline.split('/'))))
    deadline_list = [deadline_split[2],deadline_split[0],deadline_split[1],0,0,0]
    deadline = datetime(*deadline_list)
    return deadline

if __name__ == '__main__':
    main()